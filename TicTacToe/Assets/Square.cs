﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class Square : MonoBehaviour
{
    [SerializeField]
    private TicTacToe ttt;

    [SerializeField]
    private int indexX, indexY;

    private Text text;

    private void Awake()
    {
        if(ttt == null)
        {
            Debug.LogError("TicTacToe is not assigned");
        }

        text = GetComponent<Text>();
    }

    void Update()
    {
        text.text = ttt.board[indexX, indexY];
    } 
    public void Selected()
    {
        if(ttt.board[indexX, indexY] == "")
        ttt.SelectedSquare(indexX, indexY);
    }
}
