﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public class GridSpace
    {
        public int row;
        public int column;
        public int player;

        public GridSpace(int row, int column)
        {
            this.row = row;
            this.column = column;
            this.player = 0;
    }

        public GridSpace(int row, int column, int player)
        {
            this.row = row;
            this.column = column;
            this.player = player;
        }
    }

    public class Node
    {
        public GridSpace move;
        public int score;

        public Node(GridSpace move, int score)
        {
            this.move = move;
            this.score = score;
        }
    }

    public const string Tag = "GameController"; 

    public string Player1 = "X";
    public string Player2 = "O";

    [SerializeField]
    private int startingPlayer = 1;
    [SerializeField]
    private GameObject gameOverPanel;
    private Text gameOverText;

    private GridSpace[,] grid;
    private List<Node> potentialNodeList;
    private int currentPlayer;
    //private int pointsAvailable;
    private int point;

    private void Awake()
    {
        if (gameOverPanel == null)
        {
            Debug.LogError("Game Over Panel is not assigned in the Inspector");
        }
        else
        {
            gameOverText = gameOverPanel.GetComponentInChildren<Text>();
            if (gameOverText == null)
            {
                Debug.LogError("There is no Text component on a child of the Game Over Panel");
            }
        }

        grid = new GridSpace[3, 3];
        currentPlayer = startingPlayer;
    }

    private void Start()
    {
        if (currentPlayer == 2)
        {
            AIMove();
        }
    }

    /// <summary>
    /// Returns the Node at given row and column
    /// </summary>
    /// <param name="row">Row of Node to return</param>
    /// <param name="column">Column of Node to return</param>
    /// <returns></returns>
    public GridSpace GetGridSpace(int row, int column)
    {
        return grid[row, column];
    }

    /// <summary>
    /// Fill the grid space at the given row and column if it is a valid move
    /// </summary>
    /// <param name="row">Row of grid space to fill</param>
    /// <param name="column">Column of grid space to fill</param>
    public void SelectSpace(int row, int column)
    {
        if(CheckValidMove(row,column))
        {
            GridSpace node = new GridSpace(row, column, currentPlayer);
            grid[row, column] = node;

            if (GetAvailableMoves().Count == 0)
            {
                gameOverPanel.SetActive(true);
                gameOverText.text = "Draw";
            }
            else
            {
                int winner = CheckWinner();
                if (winner != 0)
                {
                    gameOverPanel.SetActive(true);
                    gameOverText.text = string.Format("Player {0} Wins!", currentPlayer);
                }
                else
                {
                    currentPlayer = currentPlayer == 1 ? 2 : 1;
                    if (currentPlayer == 2)
                    {
                        AIMove();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Checks if a grid space has been filled
    /// </summary>
    /// <param name="row">Row of grid space to check</param>
    /// <param name="column">Column of grid space to check</param>
    /// <returns>True if the space is not filled</returns>
    private bool CheckValidMove(int row, int column)
    {
        return GetGridSpace(row, column) == null;
    }

    /// <summary>
    /// Returns a list of availables moves
    /// </summary>
    /// <returns>List of Nodes representing the available moves</returns>
    private List<GridSpace> GetAvailableMoves()
    {
        List<GridSpace> result = new List<GridSpace>();

        for (int row = 0; row < 3; row++)
        {
            for (int column = 0; column < 3; column++)
            {
                if (CheckValidMove(row, column))
                {
                    GridSpace newNode = new GridSpace(row, column);
                    result.Add(newNode);
                }
            }
        }

        return result;
    }

    /// <summary>
    /// Returns the winning player
    /// </summary>
    /// <returns>1 if Player 1, 2 if Player 2, 0 if no winner yet</returns>
    private int CheckWinner()
    {
        for(int i = 0; i < 3; i++)
        {
            // Rows
            if(grid[i, 0] != null && grid[i, 1] != null && grid[i, 2] != null &&
               grid[i, 0].player == grid[i, 1].player &&
               grid[i, 0].player == grid[i, 2].player)
            {
                return grid[i, 0].player;
            }

            // Columns
            if (grid[0, i] != null && grid[1, i] != null && grid[2, i] != null &&
              grid[0, i].player == grid[1, i].player &&
              grid[0, i].player == grid[2, i].player)
            {
                return grid[0, i].player;
            }
        }

        // Diagonals
        if (grid[0, 0] != null && grid[1, 1] != null && grid[2, 2] != null &&
              grid[0, 0].player == grid[1, 1].player &&
              grid[0, 0].player == grid[2, 2].player)
        {
            return grid[0, 0].player;
        }
        if (grid[2, 0] != null && grid[1, 1] != null && grid[0, 2] != null &&
              grid[2, 0].player == grid[1, 1].player &&
              grid[2, 0].player == grid[0, 2].player)
        {
            return grid[2, 0].player;
        }

        return 0;
    }

    /// <summary>
    /// Generate the possible moves and select the best one for the AI to do
    /// </summary>

    //returns the minimum element of the list passed to it
    public int returnMin(List<int> list) {
		int min = 100000;
		int index = -1;
		for (int i = 0; i < list.Count; ++i) {
			if (list[i] < min) {
				min = list[i];
				index = i;
			}
		}
		return list[index];
	}

	//returns the maximum element of the list passed to it
	public int returnMax(List<int> list) {
		int max = -100000;
		int index = -1;
		for (int i = 0; i < list.Count; ++i) {
			if (list[i] > max) {
				max = list[i];
				index = i;
			}
		}
		return list[index];
	}
    private void AIMove()
    {
        // Reset the node list
        potentialNodeList.Clear();

        //Call Minimax on the AI's turn
        Minimax();

        // Determine the best move from the potential moves

        // Select the best move
    }

    /// <summary>
    /// Minimax Algorithm 
    /// </summary>
    /// <param name="depth"></param>
    /// <param name="turn"></param>
    private int Minimax(int depth, int turn)
    {
        // Check if there is a winner on the current board 
        CheckWinner();

        // If the winner is the player assign a negative value
        if (currentPlayer == 1)
        return -1;

        // If the winner is the AI assign a positive value
        if (currentPlayer == 2)
        return +1;

        // Get the available moves check to see how many remain
        GetAvailableMoves();

        // Return 0 if there are no more moves (game ended in a draw)
        if (GetAvailableMoves().Count == 0)
            {
                return 0;
            }

        List<GridSpace> pointsAvailable = GetAvailableMoves();

        if (pointsAvailable.Capacity == 0)
            return 0;

        // Create a list to store the scores
        List<int> scores = new List <int>();

        return 0;

        for (int i = 0; i < pointsAvailable.Count; i++)
            GridSpace move = pointsAvailable[i];

        if (turn == 1)
        {
            GridSpace x = new GridSpace();
            x.row = point.row;
            x.column = point.column;
            x.player = 2;
            grid[point.row,point.column] = x;

            int score = Minimax(depth + 1, 2);
            scores.Add(score);

            if(depth == 0)
            {
                GridSpace m = new GridSpace(score, point);
                m.point = point;
                m.score = score;
                scores.Add(score);
            }
        }
        else if (turn == 2)
            {
				GridSpace o = new GridSpace();
				o.row = point.row;
				o.column = point.colomn;
				o.player = 1;
				grid[point.row, point.colomn] = o;
				int currentScore = Minimax(depth+1,1);
				scores.Add(currentScore);
			}
			grid[point.row, point.colomn] = null; //reset the point

		return turn == 1 ? returnMax(scores) : returnMin(scores);
	}
}